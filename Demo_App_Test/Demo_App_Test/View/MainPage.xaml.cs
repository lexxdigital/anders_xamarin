﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Demo_App_Test.View
{
    public partial class MainPage : ContentPage
    {
        public static readonly string Desc_data = "This shoe is cool and it is almost not used. I want to swop it because Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,";
        private int position;

        public int Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                OnPropertyChanged();
            }
        }
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            carousel.ItemsSource = new[] { "adidas_img.png", "adidas_img.png", "adidas_img.png" };
            indicator.ItemsSource = new[] { "adidas_img.png", "adidas_img.png", "adidas_img.png" };
            BindingContext = this;
        }

    }
}
