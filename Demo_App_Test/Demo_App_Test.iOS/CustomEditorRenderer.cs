﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Demo_App_Test.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Demo_App_Test.Renderer.CustomEditor), typeof(CustomEntryRenderer))]

namespace Demo_App_Test.iOS
{
    class CustomEntryRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            Control.BackgroundColor = UIColor.Clear;
        }
    }
}